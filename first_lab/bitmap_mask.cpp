#include <stdio.h>

// Function to set a specific bit at position 'pos' in the integer 'num'
int setBit(int num, int pos) {
  return num | (1 << pos);
}

// Function to clear a specific bit at position 'pos' in the integer 'num'
int clearBit(int num, int pos) {
  return num & ~(1 << pos);
}

// Function to toggle a specific bit at position 'pos' in the integer 'num'
int toggleBit(int num, int pos) {
  return num ^ (1 << pos);
}

// Function to check if a specific bit at position 'pos' in the integer 'num' is set
int isBitSet(int num, int pos) {
  return (num >> pos) & 1;
}

int main() {
  int num = 10; // binary representation: 1010

  printf("Initial number: %d\n", num);

  // Setting bit at position 2 (0-based indexing)
  num = setBit(num, 2);
  printf("Number after setting bit at position 2: %d\n", num); // Output: 14 (binary: 1110)

  // Clearing bit at position 3
  num = clearBit(num, 3);
  printf("Number after clearing bit at position 3: %d\n", num); // Output: 6 (binary: 0110)

  // Toggling bit at position 0
  num = toggleBit(num, 0);
  printf("Number after toggling bit at position 0: %d\n", num); // Output: 7 (binary: 0111)

  // Checking if bit at position 1 is set
  if (isBitSet(num, 1))
    printf("Bit at position 1 is set\n");
  else
    printf("Bit at position 1 is not set\n");

  return 0;
}
