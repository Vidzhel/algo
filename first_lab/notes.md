

## Вступ
- Ціль курсу
- Ціль лабораторної - попрацювати з типами даних, в тому числі складними, побачити як це працює в пам'яті

## Прості змінні
https://en.wikipedia.org/wiki/C_data_types
https://www.geeksforgeeks.org/representation-of-int-variable-in-memory-in-cpp/
https://www.allmath.com/twos-complement.php
https://www.h-schmidt.net/FloatConverter/IEEE754.html
```c
int main() {
  // змінна має вказівник на розташування у пам'яті та розмір цієї пам'яті, 
  // який визначається типом. Тип також вказує на те як поводити себе з даною частиною пам'яті
  
  // Цілі
  char a = 10; // signed char (8 bits) -128 - 127
  a = -1; // 0xff - негативні числа відображаються шляхом "доповнення до двох"

  short b = 10; // signed short (16 bits) -32,768 - 32,767
  b = 256; // дане число займає 9 біт, що більше одного байта,
  // в залежності від big/little endian процесора, порядок байтів може бути зліва направо, чи справа наліво,
  // тобто може бути або 0x0001 або 0x0100 відповідно
  // int, long, long long...

  // переповнення
  unsigned char un_a = 10; // 0 - 255
  a = 127;
  a += 1; // -128
  un_a = 255;
  un_a += 1; // 0

  // Інші
  float c = 1.23; // в чисел з плаваючою точкою є свою точність, по цій причині їх не використовують щоб рахувати гроші...
  bool d = true;
  enum days_of_week {
      Mon,
      Tue,
      Wed,
      Thu,
      Fri,
      Sat,
      Sun
  };
  days_of_week day = Fri;
  
  // Побітові операції
  a = -1;
  a = ~a; // побітове не
  a = 1;
  a = a << 1; // зсув усіх бітів вліво або множення на 2
  a = a >> 1;

  return 0;
}
```

## Складені типи

```c
int main() {
  // Масиви
  char string[] = "Hello world"; // ascii символи або utf-8 вміщують букву латинського алфавіту в один байт, тому і char-acter
  // масив зберігається в пам'яті послідовно

  // структури
  struct {
      char firstField;
      char secondField;
  } simple_struct{};
  simple_struct.secondField = -1;

  // бітові поля
  // якщо в нас є обмеження по пам'яті, то ми можемо більш ощадливо використовувати місце
  struct {
      char firstField: 4;
      char secondField: 4;
  } char_sized_struct{};
  char_sized_struct.firstField = 0xA; // перша частина char
  char_sized_struct.secondField = 0x0; // друга частина char
  char* char_view = (char*)&char_sized_struct;

  // зазвичай використовується з одним типом, бо інакше результат може бути неочікуваним
  struct {
      char firstField: 3;
      short secondField: 13;
  } different_types_example{};
  different_types_example.firstField = 0x6; // перша частина char
  different_types_example.secondField = 0x0; // друга частина char
  short* short_view = (short*)&different_types_example;

  return 0;
}
```

## Unions
- Дозволяють зберігати декілька змінних в одній частині пам'яті

```c
#include <iostream>

union {
    short value;
    unsigned char firstPart; // має ту ж адресу що й value, однак вдвічі менший розмір
} numberView; // має довжину найбільшого із членів

int main() {
    printf("first: %p, %p", &numberView.value, &numberView.firstPart);
    scanf("%hi", &numberView.value);
    return 0;
}
```

```c
union intParts {
int theInt;
char bytes[sizeof(int)];
};


int main() {
union intParts parts;
parts.theInt = 5968145; // arbitrary number > 255 (1 byte)

printf("The int is %i\nThe bytes are [%i, %i, %i, %i]\n",
parts.theInt, parts.bytes[0], parts.bytes[1], parts.bytes[2], parts.bytes[3]);

// vs

int theInt = parts.theInt;
printf("The int is %i\nThe bytes are [%i, %i, %i, %i]\n",
theInt, *((char*)&theInt+0), *((char*)&theInt+1), *((char*)&theInt+2), *((char*)&theInt+3));

// or with array syntax which can be a tiny bit nicer sometimes

printf("The int is %i\nThe bytes are [%i, %i, %i, %i]\n",
    theInt, ((char*)&theInt)[0], ((char*)&theInt)[1], ((char*)&theInt)[2], ((char*)&theInt)[3]);
return 0;
}
```
