#include <stdio.h>
#include <stdlib.h>

int main() {
  system("chcp 65001");
  short int num;

  printf("Введіть ціле число типу signed short: ");
  scanf("%hi", &num);

  // Отримання знаку
  int sign = (num >> (sizeof(short) * 8 - 1)) & 1;

  // Отримання значення (відкидання бітів знаку)
  unsigned short int value = num & ~(1 << (sizeof(short) * 8 - 1));

  // Виведення результатів
  printf("Значення: %hi\n", value);
  printf("Знак: %s\n", sign ? "від'ємне" : "позитивне");

  return 0;
}
