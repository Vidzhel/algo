#include <stdio.h>

union CharBitViewer {
    struct {
        unsigned char reminder: 7;
        unsigned char sign: 1;
    } bits;
    char value;
} num; // розміром в 1 байт (char)

int main() {
  scanf("%hi", &num.value);
  return 0;
}
