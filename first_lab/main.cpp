int main() {
  // Масиви
  char string[] = "Hello world"; // ascii символи або utf-8 вміщують букву латинського алфавіту в один байт, тому і char-acter
  // масив зберігається в пам'яті послідовно

  // структури
  struct {
      char firstField;
      char secondField;
  } simple_struct{};
  simple_struct.secondField = -1;

  // бітові поля
  // якщо в нас є обмеження по пам'яті, то ми можемо більш ощадливо використовувати місце
  struct {
      char firstField: 4;
      char secondField: 4;
  } char_sized_struct{};
  char_sized_struct.firstField = 0xA; // перша частина char
  char_sized_struct.secondField = 0x0; // друга частина char
  char* char_view = (char*)&char_sized_struct;

  // зазвичай використовується з одним типом, бо інакше результат може бути неочікуваним
  struct {
      char firstField: 3;
      short secondField: 13;
  } different_types_example{};
  different_types_example.firstField = 0x6; // перша частина char
  different_types_example.secondField = 0x0; // друга частина char
  short* short_view = (short*)&different_types_example;

  return 0;
}
