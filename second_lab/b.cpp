#include <stdio.h>
#include <stdint.h>
#include <math.h>

#define A 1664525
#define C 1013904223
#define M 4294967296 // 2^32
#define NUMBERS_TO_GENERATE 1000000 // Кількість чисел для генерації
#define INTERVAL_MAX 100 // Максимальне значення інтервалу

uint32_t current_seed = 0;

void srand(uint32_t seed) {
  current_seed = seed;
}

uint32_t rand() {
  current_seed = (A * current_seed + C) % M;
  return current_seed;
}

int main() {
  srand(1234); // Ініціалізація генератора

  int intervals[INTERVAL_MAX] = {0}; // Масив для підрахунку інтервалів
  double sum = 0;
  double sum_squares = 0;
  double mean, variance, stddev;

  // Генерація випадкових чисел та підрахунок інтервалів
  for (int i = 0; i < NUMBERS_TO_GENERATE; i++) {
    uint32_t number = rand() % INTERVAL_MAX; // Обмеження числа інтервалом [0, INTERVAL_MAX-1]
    intervals[number]++;
    sum += number;
    sum_squares += (double)number * number;
  }

  // Обчислення математичного сподівання
  mean = sum / NUMBERS_TO_GENERATE;

  // Обчислення дисперсії
  variance = (sum_squares / NUMBERS_TO_GENERATE) - (mean * mean);

  // Обчислення середньоквадратичного відхилення
  stddev = sqrt(variance);

  // Виведення результатів
  printf("Ймовірність інтервалів:\n");
  for (int i = 0; i < INTERVAL_MAX; i++) {
    printf("Інтервал %d: %f\n", i, (double)intervals[i] / NUMBERS_TO_GENERATE);
  }

  printf("\nМатематичне сподівання: %f\n", mean);
  printf("Дисперсія: %f\n", variance);
  printf("Середньоквадратичне відхилення: %f\n", stddev);

  return 0;
}
