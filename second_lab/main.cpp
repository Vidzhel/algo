#include <stdio.h>
#include <stdint.h>

// Константи для ЛКМ
#define A 1664525
#define C 1013904223
#define M 4294967296 // 2^32

// Глобальна змінна для збереження поточного стану генератора
uint32_t current_seed = 0;

// Функція ініціалізації генератора заданим початковим значенням (зерном)
void srand(uint32_t seed) {
  current_seed = seed;
}

// Функція генерації наступного випадкового числа
uint32_t rand() {
  current_seed = (A * current_seed + C) % M;
  return current_seed;
}

int main() {
  // Ініціалізація генератора
  srand(1234); // Можна використати будь-яке зерно

  // Генерація та друк 10 випадкових чисел
  for (int i = 0; i < 10; i++) {
    printf("%u\n", rand());
  }

  return 0;
}
